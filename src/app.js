// const express = require('express');
// const graphHTTP = require('express-graphql');
// const schema = require('../schema/schema');
import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import graphHTTP from "express-graphql";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import schema from "./schema/schema";
import { MONGO_URI } from "./utils/constants";
// import models from '../src/models';

const SECRET_KEY = "jfsdourw0729056023--10";

mongoose
  .connect(MONGO_URI)
  .then(() => console.log("DB connected successfully"))
  .catch(err => console.log(err));

// Allow cross-origin requests

const app = express();
app.use(cors('*'));

app.get("/", (req, res) => {
  res.send("Hello");
});

app.listen(4000, () => {
  console.log("Server is running on port 4000");
});

const verifyAuthor = async (req,res,next) => {
  const token = req.headers.authorization;
  console.log(token);
 try {
   const name = await jwt.verify(token, SECRET_KEY);
 
   req.author = name;

 } catch (ex) {
   console.log('Error message', ex);
 }

  next();
};

app.use(verifyAuthor);

app.use(
  "/graphql",
  bodyParser.json(),
  verifyAuthor,
  graphHTTP(req => ({
    schema: schema,
    graphiql: true,
    context: {
      SECRET_KEY,
      author: req.author
    }
  }))
);
