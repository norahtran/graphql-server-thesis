const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  name: String,
  genere: String,
  authorId: String
});

export default mongoose.model('Book', BookSchema);