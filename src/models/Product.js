const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const ProductSchema = new Schema({
  name: String,
  category: String,
  classification: String,
  location: String,
  farmerId: String
});

export default mongoose.model("Product", ProductSchema);
