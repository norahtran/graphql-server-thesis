import mongoose from 'mongoose';
import Product from './Product';

const Schema = mongoose.Schema;
const FarmerSchema = new Schema({
  name: String,
  address: String,
  age: String,
  // products: [Product]
});

export default mongoose.model('Farmer', FarmerSchema);