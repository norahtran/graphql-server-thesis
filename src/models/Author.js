const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Authorchema = new Schema({
  name: String,
  age: Number
});

export default mongoose.model('Author', Authorchema);