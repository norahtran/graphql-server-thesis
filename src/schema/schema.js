import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLList,
  GraphQLSchema,
  GraphQLInt,
  GraphQLBoolean
} from "graphql";
import _ from "lodash";
import Author from "../models/Author";
import jwt from "jsonwebtoken";
import User from "../models/User";
import Product from "../models/Product";
import Farmer from "../models/Farmer";

const products = [
  {
    id: "1",
    name: "tomato",
    category: "",
    classification: "A",
    location: "Dalat",
    farmerId: "1",
    grownPeriod: ""
  },
  {
    id: "2",
    name: "apple",
    category: "",
    classification: "A",
    location: "Dalat",
    farmerId: "2",
    grownPeriod: ""
  },
  {
    id: "3",
    name: "mango",
    category: "",
    classification: "A",
    location: "Dalat",
    farmerId: "3",
    grownPeriod: ""
  },
  {
    id: "3",
    name: "orange",
    category: "",
    classification: "A",
    location: "Dalat",
    farmerId: "2",
    grownPeriod: ""
  }
];

const farmers = [
  {
    id: "1",
    name: "John",
    address: "",
    productIds: ["1", "2", "3"]
  },
  {
    id: "2",
    name: "Bubby",
    address: "",
    productIds: ["1", "2", "3"]
  },
  {
    id: "3",
    name: "Leggy",
    address: "",
    productIds: ["1", "2", "3"]
  }
];

const ProductType = new GraphQLObjectType({
  name: "Product",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    category: { type: GraphQLString },
    classification: { type: GraphQLString },
    location: { type: GraphQLString },
    farmerId: { type: GraphQLID },
    farmer: {
      type: FarmerType,
      resolve(parent, args) {
        const farmer = Farmer.findById(parent.farmerId);
        return farmer;
      }
    }
  })
});

const FarmerType = new GraphQLObjectType({
  name: "Farmer",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    age: { type: GraphQLInt },
    address: { type: GraphQLString },
    products: {
      type: new GraphQLList(ProductType),
      resolve(parent, args) {
        // return products.filter((product) => product.farmerId === parent.id);
        return _.filter(products, { farmerId: parent.id });
      }
    }
  })
});

const UserType = new GraphQLObjectType({
  name: "User",
  fields: () => ({
    username: { type: GraphQLString },
    password: { type: GraphQLString }
  })
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    products: {
      type: new GraphQLList(ProductType),
      resolve(parent, args) {
        return products;
      }
    },
    product: {
      type: ProductType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return products.find(product => product.id === args.id);
      }
    },
    farmer: {
      type: FarmerType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return farmers.find(farmer => farmer.id === args.id);
      }
    },
    farmers: {
      type: new GraphQLList(FarmerType),
      resolve(parent, args) {
        return farmers;
      }
    },
    getAuthor: {
      type: GraphQLString,
      resolve(parent, args, { models, author, SECRET_KEY }) {
        const token = jwt.sign(
          {
            name: "bob"
          },
          SECRET_KEY,
          {
            expiresIn: "1d"
          }
        );
        return token;
      }
    },
    verifyAuthor: {
      type: GraphQLString,
      resolve(parent, args, { author }) {
        console.log(author);
        if (author) {
          return "cool";
        }
        return "not great";
      }
    },
    login: {
      type: GraphQLString,
      args: {
        username: { type: GraphQLString },
        password: { type: GraphQLString }
      },
      async resolve(parent, args, { SECRET_KEY }) {
        let user = await User.findOne({
          username: args.username
        });
        if (user && user.password === args.password) {
          return jwt.sign(
            {
              username: user.username,
              userId: user.id
            },
            SECRET_KEY
          );
        }
        return "";
      }
    },

    filterBy: {
      type: new GraphQLList(ProductType),
      args: {
        name: { type: GraphQLString },
        classification: { type: GraphQLString },
        category: { type: GraphQLString },
        location: { type: GraphQLString }
      },
      async resolve(parent, args, {}) {
        const products = await Product.find().or([{name: args.name},{classification: args.classification}]);
        // .where('classfication').equals(args.classification);
        console.log(args.classification);
        console.log(products);
        return products;
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: () => ({
    addAuthor: {
      type: AuthorType,
      args: {
        name: { type: GraphQLString },
        age: { type: GraphQLInt }
      },
      resolve(parent, args, { SECRET_KEY }) {
        let author = new Author({
          name: args.name,
          age: args.age
        });

        return author.save();
        // return author;
      }
    },

    registerUser: {
      type: UserType,
      args: {
        username: { type: GraphQLString },
        password: { type: GraphQLString }
      },
      resolve(parent, args) {
        let user = new User({
          username: args.username,
          password: args.password
        });
        return user.save();
      }
    },

    addProduct: {
      type: ProductType,
      args: {
        name: { type: GraphQLString },
        classification: { type: GraphQLString },
        category: { type: GraphQLString },
        location: { type: GraphQLString },
        farmerId: { type: GraphQLID }
      },
      resolve(parent, args, {}) {
        let product = new Product({
          name: args.name,
          classification: args.classification,
          category: args.category,
          location: args.location,
          farmerId: args.farmerId
        });
        return product.save();
      }
    },

    addFarmer: {
      type: FarmerType,
      args: {
        name: { type: GraphQLString },
        age: { type: GraphQLInt },
        address: { type: GraphQLString }
      },
      resolve(parent, args, {}) {
        let farmer = new Farmer({
          name: args.name,
          age: args.age,
          address: args.address
        });
        return farmer.save();
      }
    }
  })
});

const AuthorType = new GraphQLObjectType({
  name: "Author",
  fields: () => ({
    name: { type: GraphQLString },
    age: { type: GraphQLInt }
  })
});

export default new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
